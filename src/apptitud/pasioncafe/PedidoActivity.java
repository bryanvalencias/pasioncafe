package apptitud.pasioncafe;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import apptitud.extras.BaseDatos;
import apptitud.extras.Conection;
import apptitud.extras.Conection.OnResponseSetPedido;
import apptitud.extras.Pedido;

public class PedidoActivity extends Activity implements OnResponseSetPedido {

	private TableLayout tableLayout;
	private TextView totalValor;
	BaseDatos db;
	ArrayList<Pedido> listaPedidos;
	SharedPreferences sharedpreferences;
	private int numeroMesa;
	private String nombreMesa;
	private int totalValorPedido = 0;
	public static Activity pedidoActivity;
	Conection conn;
	NumberFormat format;
	Toast toast = null;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_pedido);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(this.getResources()
				.getColor(R.color.color_blanco)));
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView abTitle = (TextView) findViewById(titleId);
		abTitle.setTextColor(this.getResources().getColor(
				R.color.color_vino_tinto_oscuro));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		tableLayout = (TableLayout) this.findViewById(R.id.tableLayout);
		totalValor = (TextView) this.findViewById(R.id.tv_total_pedido);
		listaPedidos = new ArrayList<Pedido>();
		Currency COP = Currency.getInstance("COP");
		format = NumberFormat.getCurrencyInstance();
		format.setCurrency(COP);
		pedidoActivity = this;
		toast = Toast.makeText(this, R.string.mensaje_enviando,
				Toast.LENGTH_LONG);

		conn = new Conection(this);
		db = new BaseDatos(this);
		db.Open();
		listaPedidos = db.getPedidosFromDb();
		db.Close();
		sharedpreferences = getSharedPreferences(Home.MyPREFERENCES,
				Context.MODE_PRIVATE);

		if (sharedpreferences.contains(Home.numberMesa)) {
			numeroMesa = sharedpreferences.getInt(Home.numberMesa, -1);
		}
		if (sharedpreferences.contains(Home.nameMesa)) {
			nombreMesa = sharedpreferences.getString(Home.nameMesa, null);
		}

		this.fillTable();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;
		}
		return (super.onOptionsItemSelected(item));
	}

	private String setDetalleProducto(int i) {
		String detalleProducto = null;
		String nombreProducto = "Nombre: "
				+ listaPedidos.get(i).producto.nombre;
		;
		String cantidad = " (" + listaPedidos.get(i).cantidad + ")";
		String presentacion = "";
		String sabor = "";
		String preparacion = "";

		if (!listaPedidos.get(i).presentacion.equals(MainActivity.no_aplica)) {
			presentacion = "\n Presentación: "
					+ listaPedidos.get(i).presentacion;
		}
		if (!listaPedidos.get(i).sabor.equals(MainActivity.no_aplica)) {
			sabor = "\n Sabor: " + listaPedidos.get(i).sabor;
		}
		if (!listaPedidos.get(i).preparacion.equals(MainActivity.no_aplica)) {
			preparacion = "\n Preparación: " + listaPedidos.get(i).preparacion;
		}

		detalleProducto = nombreProducto + cantidad + presentacion + sabor
				+ preparacion;
		return detalleProducto;
	}

	private void fillTable() {

		for (int i = 0; i < listaPedidos.size(); i++) {

			TableRow tableRow = new TableRow(this);
			tableRow.setLayoutParams(new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			TextView tv = new TextView(this);
			tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT, 1.55f));
			tv.setPadding(5, 5, 5, 5);
			tv.setTextSize(18);
			tv.setText(setDetalleProducto(i));
			tableRow.addView(tv);
			tableRow.setDividerDrawable(new ColorDrawable(
					R.color.color_naranga_oscuro));
			TextView tv2 = new TextView(this);
			tv2.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT, 1f));
			tv2.setPadding(5, 5, 5, 5);
			tv2.setTextSize(18);
			tv2.setText(String.valueOf("$"
					+ format.format(listaPedidos.get(i).producto.precio)));
			tableRow.addView(tv2);
			tableLayout.addView(tableRow);
			totalValorPedido = totalValorPedido
					+ listaPedidos.get(i).valorDetallePedido;
		}
		totalValor
				.setText(String.valueOf("$" + format.format(totalValorPedido)));
	}

	public void btnConfirmarPedido(View v) {
		if (MainActivity.isOnline(this)) {
			final AlertDialog.Builder build = new AlertDialog.Builder(this);
			build.setTitle(R.string.confirmar);
			build.setMessage(R.string.mensaje_confirmar_pedido);

			build.setPositiveButton(this.getString(R.string.confirmar_pedido),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int position) {
							if (listaPedidos.size() > 0) {
								conn.enviarPedido(buildJSON());
								setProgressBarIndeterminateVisibility(true);
								toast.setText(R.string.mensaje_enviando);
								toast.show();
							} else {
								toast.setText(R.string.mensaje_lista_pedido_vacio);
								toast.show();
							}
							dialog.cancel();
						}
					});
			build.setNegativeButton(this.getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			build.create();
			build.show();
		} else {
			setProgressBarIndeterminateVisibility(false);
			toast.setText(R.string.mensaje_sin_conexion);
			toast.show();

		}
	}

	private JSONObject buildJSON() {
		totalValorPedido = 0;
		JSONObject obj_pedido;
		JSONObject obj_detalle_pedido;
		JSONObject obj = new JSONObject();
		JSONArray array_obj_detalle_pedido = new JSONArray();
		try {
			if (listaPedidos.size() > 0) {
				for (Pedido i : listaPedidos) {
					obj_detalle_pedido = new JSONObject();
					obj_detalle_pedido.put("producto_id",
							i.producto.idProductos);
					obj_detalle_pedido.put("cantidad", i.cantidad);
					obj_detalle_pedido.put("sabor", i.sabor);
					obj_detalle_pedido.put("presentacion", i.presentacion);
					obj_detalle_pedido.put("preparacion", i.preparacion);
					obj_detalle_pedido.put("precio_detalle_pedido",
							i.valorDetallePedido);
					totalValorPedido = totalValorPedido + i.valorDetallePedido;
					array_obj_detalle_pedido.put(obj_detalle_pedido);
					Log.e("JSOngg", obj_detalle_pedido.toString());
				}
				obj_pedido = new JSONObject();
				obj_pedido.put("detalle_pedido", array_obj_detalle_pedido);
				obj_pedido.put("total", totalValorPedido);
				obj_pedido.put("nombre_mesa", nombreMesa);
				obj_pedido.put("numero_mesa", numeroMesa);

				obj.put(Conection.userNameKey, Conection.userNameValue);
				obj.put(Conection.userPassKey, Conection.userPassValue);
				obj.put(Conection.methodSetPedidoKey,
						Conection.methodSetPedidoValue);
				obj.put("pedido", obj_pedido);
				Log.e("JSOngg",
						"________________________________________________");
				Log.e("JSOngg", obj.toString());

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

	@Override
	public void onResponseSetPedido(JSONObject response) {
		try {
			if (response.getString("mensaje").equals("ok")) {
				listaPedidos.clear();
				db.Open();
				int i = db.deleteCategoriesToDatabase(null);
				db.Close();
				if (i > 0) {
					tableLayout.removeAllViews();
					toast.setText(R.string.mensaje_confirmar_exitoso);
					toast.show();

					if (sharedpreferences.contains(Home.nameMesa)) {
						sharedpreferences.edit().remove(Home.nameMesa).commit();
						Log.e("sharedpreferences",
								"sharedpreferences nameMesa removed");
					}
					setProgressBarIndeterminateVisibility(false);
					MainActivity.mainActivity.finish();
					Intent in = new Intent();
					in.setClass(this, ShareActivity.class);
					this.startActivity(in);
					this.finish();

				}
			} else {
				toast.setText(R.string.mensaje_confirmar_error);
				toast.show();

			}
		} catch (JSONException e) {
			setProgressBarIndeterminateVisibility(false);
			e.printStackTrace();
		}

	}

	@Override
	public void onResponseErrorSetPedido(String textError) {
		setProgressBarIndeterminateVisibility(false);
		toast.setText(R.string.mensaje_confirmar_error2 + textError);
		toast.show();

	}
}

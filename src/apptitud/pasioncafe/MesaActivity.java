package apptitud.pasioncafe;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

public class MesaActivity extends Activity {

	private EditText etNumeroMesa;
	SharedPreferences sharedpreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_mesa);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		etNumeroMesa = (EditText) this.findViewById(R.id.et_ingrese_mesa);
		sharedpreferences = getSharedPreferences(Home.MyPREFERENCES,
				Context.MODE_PRIVATE);
	}

	public void btnIngresarNumeroMesa(View v) {
		if (etNumeroMesa.getText().length() > 0) {
			Editor editor = sharedpreferences.edit();
			editor.putBoolean(Home.key, true);
			editor.putInt(Home.numberMesa,
					Integer.valueOf(etNumeroMesa.getText().toString()));
			editor.commit();
			this.finish();

		} else {
			Toast.makeText(this, this.getString(R.string.ingresar_mesa),
					Toast.LENGTH_LONG).show();

		}
	}
}

package apptitud.pasioncafe;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import apptitud.extras.BaseDatos;

@SuppressLint("ShowToast")
public class Home extends Activity {

	private EditText nombreMesa;
	SharedPreferences sharedpreferences;
	public static final String MyPREFERENCES = "PrefsPasion";
	public static final String key = "nameKey";
	public static final String numberMesa = "numberKey";
	public static final String nameMesa = "nameMesa";
	private static int cont = 1;
	private static int tope = 5;
	Toast toast = null;
	BaseDatos db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedpreferences = getSharedPreferences(MyPREFERENCES,
				Context.MODE_PRIVATE);

		if (!sharedpreferences.getBoolean(key, false)) {
			Intent i = new Intent();
			i.setClass(this, MesaActivity.class);
			this.startActivity(i);
		}
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);
		nombreMesa = (EditText) this.findViewById(R.id.et_nombre_mesa);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		toast = Toast.makeText(this, R.string.toque, Toast.LENGTH_LONG);

	}

	public void clickIngresarNombreMesa(View v) {

		if (nombreMesa.getText().length() > 0) {
			Editor editor = sharedpreferences.edit();
			editor.putString(Home.nameMesa, nombreMesa.getText().toString());
			editor.commit();
			nombreMesa.setText("");
			db = new BaseDatos(this);
			db.Open();
			db.deleteCategoriesToDatabase(null);
			db.Close();
			Intent i = new Intent();
			i.setClass(this, MainActivity.class);
			this.startActivity(i);

		} else {
			Toast.makeText(this, this.getString(R.string.nombre_mesa_vacia),
					Toast.LENGTH_SHORT).show();
		}
	}

	public void clickImageLogoHome(View v) {

		if (cont == tope) {
			Intent i = new Intent();
			i.setClass(this, MesaActivity.class);
			this.startActivity(i);
			toast.setText(this.getString(R.string.configurar_mesa));
			cont = 1;
		} else {

			if (cont == 1) {
				toast.setText(String.valueOf(cont) + " "
						+ this.getString(R.string.toque));

			} else {

				toast.setText(String.valueOf(cont) + " "
						+ this.getString(R.string.toque));

			}
			toast.show();

			cont++;
		}
	}

}

package apptitud.pasioncafe;

import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import apptitud.extras.AdapterMenu;
import apptitud.extras.BaseDatos;
import apptitud.extras.Conection;
import apptitud.extras.Conection.OnGetProductosFromServer;
import apptitud.extras.MetodoFiltrado;
import apptitud.extras.Pedido;
import apptitud.extras.Productos;
import apptitud.extras.adapterMetodoFiltrado;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener, OnGetProductosFromServer {

	private AppSectionsPagerAdapter mAppSectionsPagerAdapter;
	private ViewPager mViewPager;
	private ProgressBar progress;
	private TextView tvError;
	public static ArrayList<Productos> productosList;
	ActionBar actionBar;
	private static String[] titles = { "Clásicas", "Modernas", "Gourmet",
			"Tu Café" };
	public static final String[] listPreparacion = { "Método filtrado",
			"Máquina espresso", };
	public static final String maquinaExpreso = "Máquina Expresso";
	public static ArrayList<MetodoFiltrado> listaMetodo;
	public static ArrayList<Pedido> pedidoList;
	public static int cantidadPedido = -1;
	public static String no_aplica = "no aplica";
	public static Activity mainActivity;
	Conection conn;
	BaseDatos db;
	static Toast toast = null;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		progress = (ProgressBar) this.findViewById(R.id.progressBar1);
		tvError = (TextView) this.findViewById(R.id.tv_Error);
		mainActivity = this;
		listaMetodo = new ArrayList<MetodoFiltrado>();
		listaMetodo.add(new MetodoFiltrado(3, "Sifón"));
		listaMetodo.add(new MetodoFiltrado(2, "Chemex"));
		listaMetodo.add(new MetodoFiltrado(2, "Prensa francesa"));
		listaMetodo.add(new MetodoFiltrado(2, "AeroPress"));
		listaMetodo.add(new MetodoFiltrado(2, "DripperCafé"));
		toast = Toast.makeText(this, R.string.mensaje_agregado_carrito,
				Toast.LENGTH_SHORT);
		actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		setProgressBarIndeterminateVisibility(true);
		pedidoList = new ArrayList<Pedido>();
		conn = new Conection(this);
		if (isOnline(this)) {
			conn.downLoad();
		} else {
			setProgressBarIndeterminateVisibility(false);
			progress.setVisibility(View.INVISIBLE);
			progress.setEnabled(false);
			tvError.setVisibility(View.VISIBLE);
			Toast.makeText(this, R.string.mensaje_sin_conexion,
					Toast.LENGTH_SHORT).show();
		}
		db = new BaseDatos(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			this.finish();
			break;
		case R.id.carrito:
			Intent i = new Intent();
			i.setClass(this, PedidoActivity.class);
			this.startActivity(i);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/** administrador de View Pager para las vistas de los Taps */
	public static class AppSectionsPagerAdapter extends
			FragmentStatePagerAdapter {
		Context context;

		public AppSectionsPagerAdapter(FragmentManager fm, Context cont) {
			super(fm);
			context = cont;
		}

		@Override
		public Fragment getItem(int i) {
			switch (i) {
			case 0:
				return new ClasicasFragment();
			case 1:
				return new ModernasFragment();
			case 2:
				return new GourmetFragment();
			default:
				return new TuCafeFragment();
			}
		}

		@Override
		public int getCount() {
			return 4;
		}

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		mViewPager.getAdapter().notifyDataSetChanged();

	}

	/**
	 * Fragmento de clasicas.
	 */
	public static class ClasicasFragment extends Fragment implements
			OnItemClickListener {
		private GridView gvClasicas;
		private AdapterMenu adapter;
		private View rootView;
		ArrayList<Productos> listaProductosClasicas;

		private Pedido pedido;
		private int PositionGrid;

		public ClasicasFragment() {
			listaProductosClasicas = MainActivity
					.clasificarProductosPorCategoria(productosList, 1);

		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			rootView = inflater.inflate(R.layout.fragment_clasicas, container,
					false);

			this.gvClasicas = (GridView) rootView
					.findViewById(R.id.gv_menu_Clasicas);
			this.adapter = new AdapterMenu(getActivity(),
					listaProductosClasicas);
			this.gvClasicas.setAdapter(adapter);
			this.gvClasicas.setOnItemClickListener(this);
			return rootView;
		}

		/**
		 * evento click de cada item del Grid view de productos de categoria
		 * clasica
		 */

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			PositionGrid = position;
			if (listaProductosClasicas.get(position).tagAmericano) {

				mostrarPreparacion();
			} else {

				mostrarNumberPicker(false);
			}

		}

		/**
		 * Muestra alerta con las opciones de preparacion este metodo solo es
		 * usado para los productos con tag americano
		 */
		private void mostrarPreparacion() {

			final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					this.getActivity(), android.R.layout.simple_list_item_1,
					listPreparacion);
			final AlertDialog.Builder alert = new AlertDialog.Builder(
					this.getActivity());
			alert.setSingleChoiceItems(adapter, -1,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int pos) {

							switch (pos) {
							case 0:
								mostrarMetodo();
								dialog.cancel();
								break;

							default:
								mostrarNumberPicker(true);
								dialog.cancel();
								break;
							}
						}
					});

			alert.show();
		}

		/**
		 * Muestra alerta con las opciones de preparacion este metodo solo es
		 * usado para los productos de categoria clasicas con tag americano
		 */
		@SuppressLint("InflateParams")
		private void mostrarMetodo() {
			final adapterMetodoFiltrado adapter = new adapterMetodoFiltrado(
					this.getActivity(), listaMetodo);
			final AlertDialog.Builder build = new AlertDialog.Builder(
					this.getActivity());
			build.setTitle(this.getActivity().getString(
					R.string.metodo_filtrado));
			build.setSingleChoiceItems(adapter, -1,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int position) {
							pedido = new Pedido();
							pedido.producto = listaProductosClasicas
									.get(PositionGrid);
							pedido.cantidad = listaMetodo.get(position).numeroTazas;
							pedido.valorDetallePedido = pedido.producto.precio
									* pedido.cantidad;
							pedido.preparacion = listaMetodo.get(position).nombre;
							pedido.presentacion = no_aplica;
							pedido.sabor = no_aplica;
							pedidoList.add(pedido);
							toast.setText(R.string.producto_agregado);
							toast.show();

							dialog.cancel();

						}
					});

			build.setNegativeButton(
					this.getActivity().getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

						}
					});
			build.create();
			build.show();
		}

		/**
		 * muestra el control para selecionar la cantidad del producto de
		 * clasicas, minimo 1 maximo 100
		 */
		@SuppressLint("InflateParams")
		private void mostrarNumberPicker(final boolean ismMaquinaExpresso) {
			final AlertDialog.Builder build = new AlertDialog.Builder(
					this.getActivity());
			LayoutInflater inflater = this.getActivity().getLayoutInflater();
			final View Inflator = inflater
					.inflate(R.layout.number_picker, null);
			build.setTitle(this.getActivity().getString(R.string.cantidad));
			build.setView(Inflator);
			final NumberPicker np = (NumberPicker) Inflator
					.findViewById(R.id.np);
			np.setMaxValue(100);
			np.setMinValue(1);
			np.setWrapSelectorWheel(false);
			build.setPositiveButton(
					this.getActivity().getString(R.string.aceptar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int position) {
							cantidadPedido = np.getValue();
							pedido = new Pedido();
							pedido.producto = listaProductosClasicas
									.get(PositionGrid);
							pedido.cantidad = cantidadPedido;
							pedido.valorDetallePedido = pedido.producto.precio
									* pedido.cantidad;
							pedido.presentacion = no_aplica;
							pedido.sabor = no_aplica;
							if (ismMaquinaExpresso)
								pedido.preparacion = maquinaExpreso;
							else
								pedido.preparacion = no_aplica;

							pedidoList.add(pedido);
							toast.setText(R.string.producto_agregado);
							toast.show();

							dialog.cancel();
						}
					});
			build.setNegativeButton(
					this.getActivity().getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			build.create();
			build.show();
		}

	}

	/**
	 * Fragmento de modernas.
	 */
	public static class ModernasFragment extends Fragment implements
			OnItemClickListener {
		private GridView gvModernas;
		private AdapterMenu adapter;
		ArrayList<Productos> listaProductosModernas;
		private Pedido pedido;
		private int PositionGridModernas;

		public ModernasFragment() {
			listaProductosModernas = MainActivity
					.clasificarProductosPorCategoria(productosList, 2);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_modernas,
					container, false);
			this.gvModernas = (GridView) rootView
					.findViewById(R.id.gv_menu_modernas);
			this.adapter = new AdapterMenu(getActivity(),
					MainActivity.clasificarProductosPorCategoria(productosList,
							2));
			this.gvModernas.setAdapter(adapter);
			this.gvModernas.setOnItemClickListener(this);
			return rootView;
		}

		/**
		 * evento click de cada item del Grid view de productos de categoria
		 * modernas
		 */
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			PositionGridModernas = position;
			if (listaProductosModernas.get(position).tagAmericano) {

				mostrarPreparacion();
			} else {
				mostrarNumberPicker(false);
			}

		}

		/**
		 * Muestra alerta con las opciones de preparacion este metodo solo es
		 * usado para los productos con tag americano de la categoria Modernas
		 */
		private void mostrarPreparacion() {

			final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					this.getActivity(), android.R.layout.simple_list_item_1,
					listPreparacion);
			final AlertDialog.Builder alert = new AlertDialog.Builder(
					this.getActivity());
			alert.setSingleChoiceItems(adapter, -1,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int pos) {

							switch (pos) {
							case 0:
								mostrarMetodo();
								dialog.cancel();
								break;

							default:
								mostrarNumberPicker(true);
								dialog.cancel();
								break;
							}
						}
					});

			alert.show();
		}

		/**
		 * Muestra alerta con las opciones de preparacion este metodo solo es
		 * usado para los productos de categoria clasicas con tag americano
		 */
		@SuppressLint("InflateParams")
		private void mostrarMetodo() {

			final adapterMetodoFiltrado adapter = new adapterMetodoFiltrado(
					this.getActivity(), listaMetodo);
			final AlertDialog.Builder build = new AlertDialog.Builder(
					this.getActivity());
			build.setTitle(this.getActivity().getString(
					R.string.metodo_filtrado));
			build.setSingleChoiceItems(adapter, -1,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int position) {
							pedido = new Pedido();
							pedido.producto = listaProductosModernas
									.get(PositionGridModernas);
							pedido.cantidad = cantidadPedido;
							pedido.valorDetallePedido = pedido.producto.precio
									* pedido.cantidad;
							pedido.preparacion = listaMetodo.get(position).nombre;
							pedido.presentacion = no_aplica;
							pedido.sabor = no_aplica;
							pedidoList.add(pedido);
							toast.setText(R.string.producto_agregado);
							toast.show();

							dialog.cancel();

						}
					});

			build.setNegativeButton(
					this.getActivity().getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

						}
					});
			build.create();
			build.show();
		}

		/**
		 * muestra el control para selecionar la cantidad del producto de
		 * modernas, minimo 1 maximo 100
		 */
		@SuppressLint("InflateParams")
		private void mostrarNumberPicker(final boolean ismMaquinaExpresso) {
			final AlertDialog.Builder build = new AlertDialog.Builder(
					this.getActivity());
			LayoutInflater inflater = this.getActivity().getLayoutInflater();
			final View Inflator = inflater
					.inflate(R.layout.number_picker, null);
			build.setTitle(this.getActivity().getString(R.string.cantidad));
			build.setView(Inflator);
			final NumberPicker np = (NumberPicker) Inflator
					.findViewById(R.id.np);
			np.setMaxValue(100);
			np.setMinValue(1);
			np.setWrapSelectorWheel(false);
			build.setPositiveButton(
					this.getActivity().getString(R.string.aceptar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int position) {
							cantidadPedido = np.getValue();
							pedido = new Pedido();
							pedido.producto = listaProductosModernas
									.get(PositionGridModernas);
							pedido.cantidad = cantidadPedido;
							pedido.valorDetallePedido = pedido.producto.precio
									* pedido.cantidad;
							pedido.presentacion = no_aplica;
							pedido.sabor = no_aplica;
							if (ismMaquinaExpresso)
								pedido.preparacion = maquinaExpreso;
							else
								pedido.preparacion = no_aplica;
							pedidoList.add(pedido);
							toast.setText(R.string.producto_agregado);
							toast.show();

							dialog.cancel();
						}
					});
			build.setNegativeButton(
					this.getActivity().getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			build.create();
			build.show();
		}
	}

	/**
	 * Fragmento de gourmet.
	 */
	public static class GourmetFragment extends Fragment implements
			OnItemClickListener {
		private GridView gvCategories;
		private AdapterMenu adapter;
		ArrayList<Productos> listaProductosGourmet;
		private Pedido pedido;
		private int PositionGridGourmet;

		public GourmetFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_gourmet,
					container, false);
			this.gvCategories = (GridView) rootView
					.findViewById(R.id.gv_menu_gourmet);
			this.adapter = new AdapterMenu(getActivity(),
					MainActivity.clasificarProductosPorCategoria(productosList,
							3));
			this.gvCategories.setAdapter(adapter);
			return rootView;
		}

		/**
		 * evento click de cada item del Grid view de productos de categoria
		 * gourmet
		 */
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			PositionGridGourmet = position;
			if (listaProductosGourmet.get(position).tagAmericano) {

				mostrarPreparacion();
			} else {

				mostrarNumberPicker(false);
			}

		}

		/**
		 * Muestra alerta con las opciones de preparacion este metodo solo es
		 * usado para los productos con tag americano de la categoria Gourmet
		 */
		private void mostrarPreparacion() {

			final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					this.getActivity(), android.R.layout.simple_list_item_1,
					listPreparacion);
			final AlertDialog.Builder alert = new AlertDialog.Builder(
					this.getActivity());
			alert.setSingleChoiceItems(adapter, -1,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int pos) {

							switch (pos) {
							case 0:
								mostrarMetodo();
								dialog.cancel();
								break;

							default:
								mostrarNumberPicker(true);
								dialog.cancel();
								break;
							}
						}
					});

			alert.show();
		}

		/**
		 * Muestra alerta con las opciones de preparacion este metodo solo es
		 * usado para los productos de categoria clasicas con tag americano
		 */
		@SuppressLint("InflateParams")
		private void mostrarMetodo() {

			final adapterMetodoFiltrado adapter = new adapterMetodoFiltrado(
					this.getActivity(), listaMetodo);
			final AlertDialog.Builder build = new AlertDialog.Builder(
					this.getActivity());
			build.setTitle(this.getActivity().getString(
					R.string.metodo_filtrado));
			build.setSingleChoiceItems(adapter, -1,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int position) {
							pedido = new Pedido();
							pedido.producto = listaProductosGourmet
									.get(PositionGridGourmet);
							pedido.cantidad = cantidadPedido;
							pedido.valorDetallePedido = pedido.producto.precio
									* pedido.cantidad;
							pedido.preparacion = listaMetodo.get(position).nombre;
							pedido.presentacion = no_aplica;
							pedido.sabor = no_aplica;
							pedidoList.add(pedido);
							toast.setText(R.string.producto_agregado);
							toast.show();

							dialog.cancel();

						}
					});

			build.setNegativeButton(
					this.getActivity().getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

						}
					});
			build.create();
			build.show();
		}

		/**
		 * muestra el control para selecionar la cantidad del producto de
		 * gourmet, minimo 1 maximo 100
		 */
		@SuppressLint("InflateParams")
		private void mostrarNumberPicker(final boolean ismMaquinaExpresso) {
			final AlertDialog.Builder build = new AlertDialog.Builder(
					this.getActivity());
			LayoutInflater inflater = this.getActivity().getLayoutInflater();
			final View Inflator = inflater
					.inflate(R.layout.number_picker, null);
			build.setTitle(this.getActivity().getString(R.string.cantidad));
			build.setView(Inflator);
			final NumberPicker np = (NumberPicker) Inflator
					.findViewById(R.id.np);
			np.setMaxValue(100);
			np.setMinValue(1);
			np.setWrapSelectorWheel(false);
			build.setPositiveButton(
					this.getActivity().getString(R.string.aceptar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int position) {
							cantidadPedido = np.getValue();
							pedido = new Pedido();
							pedido.producto = listaProductosGourmet
									.get(PositionGridGourmet);
							pedido.cantidad = cantidadPedido;
							pedido.valorDetallePedido = pedido.producto.precio
									* pedido.cantidad;
							pedido.presentacion = no_aplica;
							pedido.sabor = no_aplica;
							if (ismMaquinaExpresso)
								pedido.preparacion = maquinaExpreso;
							else
								pedido.preparacion = no_aplica;

							pedidoList.add(pedido);
							toast.setText(R.string.producto_agregado);
							toast.show();
							dialog.cancel();
						}
					});
			build.setNegativeButton(
					this.getActivity().getString(R.string.cancelar),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			build.create();
			build.show();
		}
	}

	/**
	 * Fragmento de tu cafe.
	 */
	public static class TuCafeFragment extends Fragment implements
			OnClickListener {
		private Spinner spiPresentacion;
		private Spinner spiSabor;
		private Spinner spiPreparacion;
		private TextView tv_valor_presentacion_azar;
		private TextView tv_valor_sabor_azar;
		private TextView tv_valor_preparacion_azar;
		private TextView tv_click_azar;
		private ArrayList<String> listPresentacion = Conection
				.listaPresentacion();
		private ArrayList<String> listPreparacion;
		private ArrayList<String> listSabor;
		ArrayList<Productos> listaProductosTuCafe;
		private Pedido pedido;
		private int PositionGrid;
		private Random randomGenerator;
		int indicator;

		public TuCafeFragment() {
			randomGenerator = new Random();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_tucafe,
					container, false);
			spiPresentacion = (Spinner) rootView
					.findViewById(R.id.spi_presentacion);
			spiSabor = (Spinner) rootView.findViewById(R.id.spi_sabor);
			spiPreparacion = (Spinner) rootView
					.findViewById(R.id.spi_preparacion);
			tv_valor_presentacion_azar = (TextView) rootView
					.findViewById(R.id.tv_valor_presentacion_azar);
			tv_valor_preparacion_azar = (TextView) rootView
					.findViewById(R.id.tv_valor_preparacion_azar);
			tv_valor_sabor_azar = (TextView) rootView
					.findViewById(R.id.tv_valor_sabor_azar);
			tv_click_azar = (TextView) rootView
					.findViewById(R.id.tv_click_azar);
			tv_click_azar.setOnClickListener(this);

			this.llenarSpiPresentacion();

			listaProductosTuCafe = MainActivity
					.clasificarProductosPorCategoria(productosList, 4);
			pedido = new Pedido();
			pedido.producto = listaProductosTuCafe.get(PositionGrid);
			pedido.cantidad = 1;
			pedido.valorDetallePedido = pedido.producto.precio
					* pedido.cantidad;
			spiPresentacion
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							indicator = arg2;
							listPreparacion = Conection
									.listaPreparacion(indicator);
							listSabor = Conection.listaSabor(indicator);
							llenarSpiSabor();
							llenarSpiPreparacion();
							pedido.presentacion = spiPresentacion
									.getSelectedItem().toString();
							pedido.preparacion = spiPreparacion
									.getSelectedItem().toString();
							pedido.sabor = spiSabor.getSelectedItem()
									.toString();

						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});

			spiPreparacion
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							pedido.presentacion = spiPresentacion
									.getSelectedItem().toString();
							pedido.preparacion = spiPreparacion
									.getSelectedItem().toString();
							pedido.sabor = spiSabor.getSelectedItem()
									.toString();

						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});
			spiSabor.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					pedido.presentacion = spiPresentacion.getSelectedItem()
							.toString();
					pedido.preparacion = spiPreparacion.getSelectedItem()
							.toString();
					pedido.sabor = spiSabor.getSelectedItem().toString();

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});

			pedidoList.removeAll(pedidoList);
			pedidoList.add(pedido);
			return rootView;
		}

		private void llenarSpiPresentacion() {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					this.getActivity(), android.R.layout.simple_spinner_item,
					listPresentacion);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spiPresentacion.setAdapter(adapter);

		}

		private void llenarSpiSabor() {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					this.getActivity(), android.R.layout.simple_spinner_item,
					listSabor);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spiSabor.setAdapter(adapter);

		}

		private void llenarSpiPreparacion() {
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					this.getActivity(), android.R.layout.simple_spinner_item,
					listPreparacion);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spiPreparacion.setAdapter(adapter);

		}

		@Override
		public void onClick(View v) {

			int indexPresentacion = randomGenerator.nextInt(Conection
					.listaPresentacion().size());
			String itemPresentacion = Conection.listaPresentacion().get(
					indexPresentacion);
			tv_valor_presentacion_azar.setText(itemPresentacion);
			pedido.presentacion = itemPresentacion;

			int indexSabor = randomGenerator.nextInt(Conection.listaSabor(
					indexPresentacion).size());
			String itemSabor = Conection.listaSabor(indexPresentacion).get(
					indexSabor);
			tv_valor_sabor_azar.setText(itemSabor);
			pedido.sabor = itemSabor;

			int indexPreparacion = randomGenerator.nextInt(Conection
					.listaPreparacion(indexPresentacion).size());
			String itemPreparacion = Conection.listaPreparacion(
					indexPresentacion).get(indexPreparacion);
			tv_valor_preparacion_azar.setText(itemPreparacion);
			pedido.preparacion = itemPreparacion;

			pedidoList.removeAll(pedidoList);
			pedidoList.add(pedido);

		}
	}

	@Override
	public void onGetProductosFromServer(ArrayList<Productos> listProductos) {
		productosList = listProductos;
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(
				getSupportFragmentManager(), this);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						getActionBar().setSelectedNavigationItem(position);
					}
				});

		for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab().setText(titles[i])
					.setTabListener(this));
		}
		progress.setVisibility(View.INVISIBLE);
		progress.setEnabled(false);
		setProgressBarIndeterminateVisibility(false);

	}

	@Override
	public void onGetErrorFromServer(String textError) {
		progress.setVisibility(View.INVISIBLE);
		progress.setEnabled(false);
		tvError.setVisibility(View.VISIBLE);
		Toast.makeText(this, textError, Toast.LENGTH_LONG).show();
		setProgressBarIndeterminateVisibility(false);

	}

	/**
	 * Método que determina cual producto del listado de productos hacen parte a
	 * la categoria indicada: 1=Clasiscas, 2=Modernas, 3=Gourmet, 4=Tu cafe
	 */
	public static ArrayList<Productos> clasificarProductosPorCategoria(
			ArrayList<Productos> productosList, int categoria) {
		ArrayList<Productos> list;
		switch (categoria) {
		case 1:
			list = new ArrayList<Productos>();
			for (Productos productos : productosList) {
				if (productos.categoria == categoria) {
					list.add(productos);
				}
			}

			return list;
		case 2:

			list = new ArrayList<Productos>();
			for (Productos productos : productosList) {
				if (productos.categoria == categoria) {
					list.add(productos);
				}
			}

			return list;
		case 3:

			list = new ArrayList<Productos>();
			for (Productos productos : productosList) {
				if (productos.categoria == categoria) {
					list.add(productos);
				}
			}

			return list;
		case 4:

			list = new ArrayList<Productos>();
			for (Productos productos : productosList) {
				if (productos.categoria == categoria) {
					list.add(productos);
				}
			}

			return list;

		default:
			return null;
		}

	}

	public static boolean isOnline(Context ctx) {
		ConnectivityManager connMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return (networkInfo != null && networkInfo.isConnected());
	}

	public void btnEnviarCarrito(View v) {
		db.Open();
		if (pedidoList.size() > 0) {
			db.registerPedidoToDb(pedidoList);
			pedidoList.clear();
			toast.setText(R.string.mensaje_agregado_carrito);
			toast.show();
		} else {
			toast.setText(R.string.mensaje_lista_pedido_vacia);
			toast.show();

		}
		db.Close();
	}

}

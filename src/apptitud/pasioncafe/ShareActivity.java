package apptitud.pasioncafe;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import apptitud.extras.Conection;
import apptitud.extras.Conection.OnResponseSetUsuarios;

@SuppressLint("ShowToast")
public class ShareActivity extends Activity implements OnResponseSetUsuarios {
	private EditText tuEmail;
	private EditText tuFacebook;
	private EditText tuTwitter;
	String email;
	String facebook;
	String twitter;
	Conection conn;
	Toast toast = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_share);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		tuEmail = (EditText) this.findViewById(R.id.et_email);
		tuFacebook = (EditText) this.findViewById(R.id.et_facebook);
		tuTwitter = (EditText) this.findViewById(R.id.et_twitter);
		conn = new Conection(this);
		toast = Toast.makeText(this, R.string.toque, Toast.LENGTH_LONG);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;
		}
		return (super.onOptionsItemSelected(item));
	}

	public void onClickShare(View v) {
		if (tuEmail.getText().length() > 0) {
			email = tuEmail.getText().toString();
			if (tuFacebook.getText().length() > 0) {
				facebook = tuFacebook.getText().toString();
			} else {
				facebook = "";
			}
			if (tuTwitter.getText().length() > 0) {
				twitter = tuTwitter.getText().toString();
			} else {
				twitter = "";
			}
			setProgressBarIndeterminateVisibility(true);
			conn.enviarUsuario(twitter, facebook, email);
			toast.setText(this.getString(R.string.mensaje_enviando));
			toast.show();

		} else {
			toast.setText(R.string.email_vacio);
			toast.show();
		}

	}

	@Override
	public void onResponseSetUsuario(JSONObject response) {
		setProgressBarIndeterminateVisibility(false);			
		this.finish();
		toast.setText(this.getString(R.string.mensaje_datos_enviados_exitoso));
		toast.show();

	}

	@Override
	public void onResponseErrorSetUsuario(String textError) {
		setProgressBarIndeterminateVisibility(false);
		toast.setText(this.getString(R.string.mensaje_enviar_datos_error) + " "
				+ textError);
		toast.show();
	}
}

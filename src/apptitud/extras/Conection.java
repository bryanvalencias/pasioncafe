package apptitud.extras;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Conection {

	public OnGetProductosFromServer getProductosListener;
	public OnResponseSetPedido getResponseSetPedidoListener;
	public OnResponseSetUsuarios getResponseSetUsuarioListener;
	public static String userNameKey = "username";
	public static String userNameValue = "app_movil";
	public static String userPassKey = "password";
	public static String userPassValue = "pasioncaf3";
	public static String methodGetProductKey = "metodo";
	public static String methodGetProductValue = "productos";
	public static String methodSetPedidoKey = "metodo";
	public static String methodSetPedidoValue = "guardar_pedido";
	public static String methodSetUserKey = "metodo";
	public static String methodSetUserValue = "guardar_usuario";

	public static String url = "http://191.98.2.21:9000/ws/";
	public static String urlTest = "http://192.168.0.15:8000/ws/";

	public Conection() {

	}

	public Conection(OnGetProductosFromServer listener) {
		this.getProductosListener = listener;

	}

	public Conection(OnResponseSetPedido listener) {
		this.getResponseSetPedidoListener = listener;

	}

	public Conection(OnResponseSetUsuarios listener) {
		this.getResponseSetUsuarioListener = listener;

	}

	public void downLoad() {
		final AsyncHttpClient client = new AsyncHttpClient();
		final RequestParams params = new RequestParams();
		JSONObject obj = new JSONObject();

		try {
			obj.put(userNameKey, userNameValue);
			obj.put(userPassKey, userPassValue);
			obj.put(methodGetProductKey, methodGetProductValue);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		params.put("json", obj.toString());
		client.post(url, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONArray response) {
				super.onSuccess(statusCode, headers, response);
				Log.i("response", response.toString());
				JSONObject json;
				Productos pro;
				ArrayList<Productos> productosList = new ArrayList<Productos>();
				try {
					for (int i = 0; i < response.length(); i++) {
						json = new JSONObject();
						pro = new Productos();
						json = response.getJSONObject(i);
						pro.idProductos = json.getInt("producto_id");
						pro.nombre = json.getString("nombre");
						pro.descripcion = json.getString("descripcion");
						pro.categoria = json.getInt("categoria");
						pro.imagen = json.getString("imagen");
						pro.precio = json.getInt("precio");
						pro.tagAmericano = json.getBoolean("tag_americano");
						productosList.add(pro);

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				getProductosListener.onGetProductosFromServer(productosList);

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				if (errorResponse != null) {
					Log.e("JSONObject errorResponse", errorResponse.toString());
					getProductosListener.onGetErrorFromServer(errorResponse
							.toString());
				} else {
					getProductosListener
							.onGetErrorFromServer("Error en la conexión");
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				if (errorResponse != null) {
					Log.e("JSONArray errorResponse", errorResponse.toString());
					getProductosListener.onGetErrorFromServer(errorResponse
							.toString());
				} else {
					getProductosListener
							.onGetErrorFromServer("Error en la conexión");
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				if (responseString != null) {
					Log.e("String responseString", responseString);
					getProductosListener.onGetErrorFromServer(responseString);
				} else {
					getProductosListener
							.onGetErrorFromServer("Error en la conexión");
				}

			}
		});
	}

	public void enviarPedido(JSONObject obj) {
		final AsyncHttpClient client = new AsyncHttpClient();
		final RequestParams params = new RequestParams();
		params.put("json", obj.toString());
		client.post(url, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				Log.i("response", response.toString());
				getResponseSetPedidoListener.onResponseSetPedido(response);

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				if (errorResponse != null) {
					Log.e("JSONObject errorResponse", errorResponse.toString());
					getResponseSetPedidoListener
							.onResponseErrorSetPedido(errorResponse.toString());
				} else {
					getResponseSetPedidoListener
							.onResponseErrorSetPedido("Error en la conexión");
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				if (errorResponse != null) {
					Log.e("JSONArray errorResponse", errorResponse.toString());
					getResponseSetPedidoListener
							.onResponseErrorSetPedido(errorResponse.toString());
				} else {
					getResponseSetPedidoListener
							.onResponseErrorSetPedido("Error en la conexión");
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				if (responseString != null) {
					Log.e("String responseString", responseString);
					getResponseSetPedidoListener
							.onResponseErrorSetPedido(responseString);
				} else {
					getResponseSetPedidoListener
							.onResponseErrorSetPedido("Error en la conexión");
				}

			}
		});
	}

	public void enviarUsuario(String twitter, String facebook, String mail) {
		final AsyncHttpClient client = new AsyncHttpClient();
		final RequestParams params = new RequestParams();
		JSONObject obj = new JSONObject();
		JSONObject obj_usuario = new JSONObject();

		try {
			obj.put(userNameKey, userNameValue);
			obj.put(userPassKey, userPassValue);
			obj.put(methodSetUserKey, methodSetUserValue);
			obj_usuario.put("facebook", facebook);
			obj_usuario.put("twitter", twitter);
			obj_usuario.put("mail", mail);
			obj.put("cliente", obj_usuario);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		params.put("json", obj.toString());
		client.post(url, params, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONObject response) {
				super.onSuccess(statusCode, headers, response);
				if (response != null) {
					getResponseSetUsuarioListener
							.onResponseSetUsuario(response);
					Log.i("response", response.toString());
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				if (errorResponse != null) {
					getResponseSetUsuarioListener
							.onResponseErrorSetUsuario(errorResponse.toString());
					Log.e("JSONObject errorResponse", errorResponse.toString());
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				if (errorResponse != null) {
					getResponseSetUsuarioListener
							.onResponseErrorSetUsuario(errorResponse.toString());
					Log.e("JSONArray errorResponse", errorResponse.toString());
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				if (responseString != null) {
					getResponseSetUsuarioListener
							.onResponseErrorSetUsuario(responseString);
					Log.e("String responseString", responseString);
				}

			}
		});
	}

	public static ArrayList<String> listaPresentacion() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Caliente");
		list.add("Frio");

		return list;
	}

	public static ArrayList<String> listaSabor(int indicator) {
		ArrayList<String> list = new ArrayList<String>();
		if (indicator == 0) {
			list.add("Naranja");
			list.add("Chontaduro");
			list.add("Leche de coco");

		} else if (indicator == 1) {
			list.add("Frutos rojos");
			list.add("Mango");
			list.add("Banano");
		}
		return list;
	}

	public static ArrayList<String> listaPreparacion(int indicator) {
		ArrayList<String> list = new ArrayList<String>();
		if (indicator == 0) {
			list.add("Capuchino");
			list.add("Bombon");

		} else if (indicator == 1) {
			list.add("Malteada");
			list.add("Latte");

		}
		return list;
	}

	public interface OnGetProductosFromServer {
		public void onGetProductosFromServer(ArrayList<Productos> listProductos);

		public void onGetErrorFromServer(String textError);
	}

	public interface OnResponseSetPedido {
		public void onResponseSetPedido(JSONObject response);

		public void onResponseErrorSetPedido(String textError);
	}

	public interface OnResponseSetUsuarios {
		public void onResponseSetUsuario(JSONObject response);

		public void onResponseErrorSetUsuario(String textError);
	}

}

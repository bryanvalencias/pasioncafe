package apptitud.extras;

public class Pedido {
	public Productos producto;
	public int cantidad;
	public String sabor;
	public String presentacion;
	public String preparacion;
	public int valorDetallePedido;

	public Pedido() {
		producto = null;
		cantidad = -1;
		sabor = null;
		presentacion = null;
		preparacion = null;
		valorDetallePedido = -1;
	}

}

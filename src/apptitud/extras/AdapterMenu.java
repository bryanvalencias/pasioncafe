package apptitud.extras;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import apptitud.pasioncafe.R;

public class AdapterMenu extends ArrayAdapter<Productos> {

	Activity activity;
	ArrayList<Productos> arrayProductos;
	private ImageLoader imageLoader;
	NumberFormat format;

	private final static DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.logo_pasion)
			.showImageForEmptyUri(R.drawable.logo_pasion)
			.showImageOnFail(R.drawable.logo_pasion)
			.bitmapConfig(Bitmap.Config.RGB_565).cacheInMemory(true)
			.cacheOnDisk(true)
			.displayer(new RoundedBitmapDisplayer((int) 2000))
			.considerExifParams(true).build();

	public AdapterMenu(Activity activity, ArrayList<Productos> arrayProductos) {
		super(activity, R.layout.item_menu, arrayProductos);

		this.activity = activity;
		this.arrayProductos = arrayProductos;
		imageLoader = ImageLoader.getInstance();
		ImageLoader.getInstance().init(
				ImageLoaderConfiguration.createDefault(this.activity));
		Currency COP = Currency.getInstance("COP");
		format = NumberFormat.getCurrencyInstance();
		format.setCurrency(COP);

	}

	@SuppressLint("InflateParams")
	public View getView(int position, View convertView, ViewGroup parent) {

		View item = convertView;
		ViewItem vistaitem;
		if (item == null) {
			LayoutInflater inflador = activity.getLayoutInflater();
			item = inflador.inflate(R.layout.item_menu, null);
			vistaitem = new ViewItem();
			vistaitem.nombre_producto_item_menu = (TextView) item
					.findViewById(R.id.nombre_producto_item_menu);

			vistaitem.descripcion_producto_item_menu = (TextView) item
					.findViewById(R.id.descripcion_producto_item_menu);

			vistaitem.precio_producto_item_menu = (TextView) item
					.findViewById(R.id.precio_producto_item_menu);

			vistaitem.ima_producto_item = (ImageView) item
					.findViewById(R.id.ima_producto_item);

			item.setTag(vistaitem);

		} else {
			vistaitem = (ViewItem) item.getTag();
		}
		vistaitem.nombre_producto_item_menu.setText(this.arrayProductos
				.get(position).nombre);
		vistaitem.descripcion_producto_item_menu.setText(this.arrayProductos
				.get(position).descripcion);
		vistaitem.nombre_producto_item_menu.setText(this.arrayProductos
				.get(position).nombre);
		vistaitem.precio_producto_item_menu.setText("$ "
				+ String.valueOf(format.format(this.arrayProductos
						.get(position).precio)));

		imageLoader.displayImage(this.arrayProductos.get(position).imagen,
				vistaitem.ima_producto_item, options);

		return item;

	}

	public static class ViewItem {
		TextView nombre_producto_item_menu;
		TextView descripcion_producto_item_menu;
		TextView precio_producto_item_menu;
		ImageView ima_producto_item;

	}

}

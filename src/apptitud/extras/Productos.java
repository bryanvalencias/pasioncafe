package apptitud.extras;

public class Productos {
	public int idProductos;
	public String nombre;
	public String descripcion;
	public boolean tagAmericano;
	public int precio;
	public String imagen;
	public int categoria;

	/**
	 * categoria tiene 4 estados: 1=Clasiscas, 2=Modernas, 3=Gourmet, 4=Tu cafe
	 */
	public Productos() {
		this.idProductos = -1;
		this.nombre = null;
		this.descripcion = null;
		this.tagAmericano = false;
		this.precio = -1;
		this.imagen = null;
		this.categoria = -1;
	}

}

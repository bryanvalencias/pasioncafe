package apptitud.extras;

import static android.provider.BaseColumns._ID;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BaseDatos extends SQLiteOpenHelper {
	private static String DB_NAME = "db_pasion";

	// TABLES
	private static String TABLE_DETALLE_PEDIDO = "table_detalle_pedido";

	// COLUMMS TABLE_DETALLE_PEDIDO
	private static String COLUMN_ID_PRODUCTO = "column_category_id_producto";
	private static String COLUMN_CANTIDAD_PRODUCTO = "column_cantidad_producto";
	private static String COLUMN_SABOR = "column_sabor";
	private static String COLUMN_PRESENTACION = "column_presentacion";
	private static String COLUMN_PREPARACION = "column_preparacion";
	private static String COLUMN_TOTAL_DETALLE_PRODUCTO = "column_total_detalle_producto";
	private static String COLUMN_VALOR_PRODUCTO_UNITARIO = "column_valor_producto_unitario";
	private static String COLUMN_NOMBRE_PRODUCTO = "column_nombre_producto";

	// QUERY CREATE TABLE CATEGORIES
	private String queryCreateTableDetallePedido = "CREATE TABLE "
			+ TABLE_DETALLE_PEDIDO + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_ID_PRODUCTO
			+ " INTEGER, " + COLUMN_CANTIDAD_PRODUCTO + " INTEGER, "
			+ COLUMN_SABOR + " TEXT NULL, " + COLUMN_PRESENTACION
			+ " TEXT NULL, " + COLUMN_PREPARACION + " TEXT NULL, "
			+ COLUMN_TOTAL_DETALLE_PRODUCTO + " INTEGER, "
			+ COLUMN_NOMBRE_PRODUCTO + " TEXT, "
			+ COLUMN_VALOR_PRODUCTO_UNITARIO + " INTEGER)";
	private String queryDrop = "DROP TABLE IF EXIST ";

	public BaseDatos(Context context) {
		super(context, DB_NAME, null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(this.queryCreateTableDetallePedido);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(queryDrop + TABLE_DETALLE_PEDIDO);
	}

	public void registerPedidoToDb(Pedido pedido) {

		ContentValues values = new ContentValues();
		values.put(COLUMN_ID_PRODUCTO, pedido.producto.idProductos);
		values.put(COLUMN_CANTIDAD_PRODUCTO, pedido.cantidad);
		values.put(COLUMN_SABOR, pedido.sabor);
		values.put(COLUMN_PRESENTACION, pedido.presentacion);
		values.put(COLUMN_PREPARACION, pedido.preparacion);
		values.put(COLUMN_TOTAL_DETALLE_PRODUCTO, pedido.valorDetallePedido);
		values.put(COLUMN_NOMBRE_PRODUCTO, pedido.producto.nombre);
		values.put(COLUMN_VALOR_PRODUCTO_UNITARIO, pedido.producto.precio);
		this.getWritableDatabase().insert(TABLE_DETALLE_PEDIDO, null, values);
		Log.i("DETALLE_PEDIDO_REGISTER_TODB", pedido.producto.nombre + " : "
				+ pedido.producto.precio + " : " + pedido.cantidad + " : "
				+ pedido.sabor + " : " + pedido.presentacion + " : "
				+ pedido.preparacion + " : " + pedido.valorDetallePedido
				+ " : " + pedido.producto.precio);
	}

	public void registerPedidoToDb(ArrayList<Pedido> pedido) {
		for (Pedido ped : pedido) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_ID_PRODUCTO, ped.producto.idProductos);
			values.put(COLUMN_CANTIDAD_PRODUCTO, ped.cantidad);
			values.put(COLUMN_SABOR, ped.sabor);
			values.put(COLUMN_PRESENTACION, ped.presentacion);
			values.put(COLUMN_PREPARACION, ped.preparacion);
			values.put(COLUMN_TOTAL_DETALLE_PRODUCTO, ped.valorDetallePedido);
			values.put(COLUMN_NOMBRE_PRODUCTO, ped.producto.nombre);
			values.put(COLUMN_VALOR_PRODUCTO_UNITARIO, ped.producto.precio);
			this.getWritableDatabase().insert(TABLE_DETALLE_PEDIDO, null,
					values);
			Log.i("DETALLE_PEDIDO_REGISTER_TODB", ped.producto.nombre + " : "
					+ ped.producto.precio + " : " + ped.cantidad + " : "
					+ ped.sabor + " : " + ped.presentacion + " : "
					+ ped.preparacion + " : " + ped.valorDetallePedido + " : "
					+ ped.producto.precio);
		}
	}

	/* Retorna el listado de pedidos almacenadas en la DB local */
	public ArrayList<Pedido> getPedidosFromDb() {
		ArrayList<Pedido> pedidoList = new ArrayList<Pedido>();
		Productos pro;
		Pedido pedi;
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_DETALLE_PEDIDO, null,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			pedi = new Pedido();
			pro = new Productos();
			pro.idProductos = cursor.getInt(1);
			pedi.producto = pro;
			pedi.cantidad = cursor.getInt(2);
			pedi.sabor = cursor.getString(3);
			pedi.presentacion = cursor.getString(4);
			pedi.preparacion = cursor.getString(5);
			pedi.valorDetallePedido = cursor.getInt(6);
			pedi.producto.nombre = cursor.getString(7);
			pedi.producto.precio = cursor.getInt(8);
			pedidoList.add(pedi);
			cursor.moveToNext();
		}
		cursor.close();
		return pedidoList;
	}

	/* counts the number of records of Companies */
	public int getCountCompanies() {
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_DETALLE_PEDIDO, null,
				null, null, null, null, null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	/* Delete TABLE_CATEGORIES */
	public int deleteCategoriesToDatabase(String IdeProducto) {
		if (IdeProducto != null) {
			return this.getWritableDatabase().delete(TABLE_DETALLE_PEDIDO,
					COLUMN_ID_PRODUCTO + "=" + IdeProducto, null);
		} else {
			return this.getWritableDatabase().delete(TABLE_DETALLE_PEDIDO,
					null, null);

		}
	}

	/* Close connection */
	public void Close() {
		this.close();
	}

	/* Open connection */
	public void Open() {
		this.getWritableDatabase();
	}

}

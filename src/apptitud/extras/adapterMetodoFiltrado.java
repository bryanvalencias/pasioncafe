package apptitud.extras;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import apptitud.pasioncafe.R;

public class adapterMetodoFiltrado extends BaseAdapter {

	private Activity activity;
	private ArrayList<MetodoFiltrado> arrayMetodos;

	public adapterMetodoFiltrado(Activity activity,
			ArrayList<MetodoFiltrado> arrayMetodos) {

		this.activity = activity;
		this.arrayMetodos = arrayMetodos;

	}

	@SuppressLint("InflateParams")
	public View getView(int position, View convertView, ViewGroup parent) {

		View item = convertView;
		ViewItem vistaitem;
		if (item == null) {
			LayoutInflater inflador = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			item = inflador.inflate(R.layout.item_menu_flitrado, null);
			vistaitem = new ViewItem();
			vistaitem.et_nombre_metodo = (TextView) item
					.findViewById(R.id.et_nombre_metodo);

			vistaitem.et_texto_numerotazas = (TextView) item
					.findViewById(R.id.et_texto_numerotazas);
			vistaitem.ima_taza_2 = (TextView) item
					.findViewById(R.id.ima_taza_2);

			vistaitem.ima_taza_3 = (TextView) item
					.findViewById(R.id.ima_taza_3);
			// vistaitem.rb_check = (RadioButton)
			// item.findViewById(R.id.rb_check);

			item.setTag(vistaitem);

		} else {
			vistaitem = (ViewItem) item.getTag();
		}
		switch (this.arrayMetodos.get(position).numeroTazas) {

		case 2:
			vistaitem.ima_taza_2.setVisibility(View.VISIBLE);
			break;
		case 3:
			vistaitem.ima_taza_2.setVisibility(View.VISIBLE);
			vistaitem.ima_taza_3.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}
		vistaitem.et_nombre_metodo
				.setText(this.arrayMetodos.get(position).nombre);
		vistaitem.et_texto_numerotazas.setText("Para "
				+ this.arrayMetodos.get(position).numeroTazas + " Tazas");

		return item;

	}

	public static class ViewItem {
		TextView et_nombre_metodo;
		TextView et_texto_numerotazas;
		TextView ima_taza_2;
		TextView ima_taza_3;
		RadioButton rb_check;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.arrayMetodos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return this.arrayMetodos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
